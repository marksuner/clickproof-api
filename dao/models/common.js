const mongoose = require('mongoose');

const userDateSchema = new mongoose.Schema({
  when: { type: Date, required: true },
  user: { type: String, required: true },
});

module.exports = {
  userDateSchema,
}