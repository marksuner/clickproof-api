const mongoose = require('mongoose');
const connection = require('../connection');

const {
  userDateSchema,
} = require('./common');

const schema = new mongoose.Schema({
  name: { type: String, required: false },
  created: { type: userDateSchema, required: true },
  updated: { type: userDateSchema, required: false },
}, {
  collection: 'users',
});

module.exports.Purpose = connection.db.model('Purpose', schema);