const config = require('config');
const mongoose = require('mongoose');

const { logger } = require('../common');

mongoose.Promise = Promise;

mongoose.set('debug', (coll, method, query, doc, options) => {
  logger.info({
    coll, method, query, doc, options,
  });
});

function createConnection(connectionConfig) {
  logger.info(connectionConfig, 'Creating connection');

  const conn = mongoose.createConnection(
    connectionConfig.uri,
    connectionConfig.options,
  );
  conn.on('connecting', () => {
    logger.info('Mongo is connecting');
  });

  conn.on('connected', () => {
    logger.info('Mongo has connected');
  });

  conn.on('error', (err) => {
    logger.fatal('Mongo is under error');
    logger.error(err);
  });

  conn.on('disconnected', () => {
    logger.info('Mongo has disconnected');
  });

  conn.on('reconnected', () => {
    logger.info('Mongo has reconnected');
  });

  process.on('SIGINT', () => {
    // interrupt captured. we're closing because we're being told to!
    conn.close(() => {
      logger.info('Mongoose default connection disconnected through app termination');
      process.exit(0);
    });
  });

  return conn;
}

module.exports = {
  db: createConnection(config.get('db.clickproof')),
};