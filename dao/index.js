const { db } = require('./connection');
const models = require('./models');

module.exports = {
  connection: db,
  models
}