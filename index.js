const { logger } = require('./common');
const server = require('./server');

server.listen(8080, function() {
  logger.info(`${server.name} listening at ${server.url}`);
});