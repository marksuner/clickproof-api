/**
 * Bunyan logger implementation
 * @module logger
 */

 const bunyan = require('bunyan');
 const restifyErrors = require('restify-errors');
 const packageJson = require('../package.json');

 // create the base logger configuration
 const loggerOpts = {
   name: packageJson.name,

   serializers: {
     req: bunyan.stdSerializers.req,
     res: bunyan.stdSerializers.res,
     err: restifyErrors.bunyanSerializer,
   },

   streams: [{
     level: process.env.LOG_LEVEL || 'info',
     stream: process.stdout,
   }],
 };

 const logger = bunyan.createLogger(loggerOpts);
 // turn off the logger when we want it muted
 if (process.env.LOG_MUTE == 1) {
   logger.level(bunyan.FATAL + 1);
 }

 module.exports = logger;