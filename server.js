const restify = require('restify');
const corsMiddleware = require('restify-cors-middleware');
const routes = require('./routes');
const packageJson = require('./package.json');
const config = require('config');

const server = restify.createServer({
  name: packageJson.name,
  version: packageJson.version,

  acceptable: ['application/json'],
});

const cors = corsMiddleware({
  origins: ['*'],
  allowHeaders: ['accept', 'accept-version', 'content-type', 'api-version', 'origin', 'x-requested-with', 'authorization'],
  exposeHeaders: ['accept', 'accept-version', 'content-type', 'api-version', 'origin', 'x-requested-with', 'authorization'],
});

server.pre(cors.preflight);
server.use(restify.plugins.acceptParser(server.acceptable));
server.use(restify.plugins.authorizationParser());
server.use(cors.actual);
server.use(restify.plugins.dateParser());
server.use(restify.plugins.queryParser());
server.use(restify.plugins.jsonp());
server.use(restify.plugins.gzipResponse());
server.use(restify.plugins.bodyParser());

// i don't think we need tho
// server.use(restify.plugins.throttle(config.get('throttle')));

server.use(restify.plugins.conditionalRequest());

// start of routes
server.get('/users', routes.users.getUsers);

module.exports = server;