function getUsers(req, res, next) {
  res.send([
    {
      id: '11235',
      name: 'Doe',
    },
    {
      id: '11254',
      name: 'John',
    },
  ])

  return next();
}

module.exports = {
  getUsers,
};